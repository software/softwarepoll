# softwarePoll - Vote for a software to be installed/updated in the Vital-IT software stack.
The purpose of this git repo is to host an HTML page that will be used to
redirect traffic from a fixed URL on https://www.vital-it.ch/services/software
to the actual poll on pollUnit


# Archive of poll results.
**Poll #1**  
https://pollunit.com/en/polls/mLNt6QodHOOTgSwC4NJgUw  
19 votes and 7 software proposed.  
software installed: replong (https://github.com/ruiguo-bio/replong)  

**Poll #2**  
https://pollunit.com/en/polls/qrVyPnGyPwcTZTGcgh251w  
https://pollunit.com/en/polls/AAT3TvwpIC0B5SqRfTibVw [admin link]  
3 votes and 5 software proposed.  
software installed: vg (genome variation graph). https://github.com/vgteam/vg 

**Poll #3**  
https://pollunit.com/en/polls/UZZPtNqIkkAy4KK0lYN01w  
https://pollunit.com/en/polls/Yi-mVYQPbSjGGl3XjJf-ng [admin link]  
14 votes and 7 software proposed.  
software installed: fastStructure https://rajanil.github.io/fastStructure/

**Poll #4**  
https://pollunit.com/en/polls/FNUcyRAci1IWjUWr-de64g  
https://pollunit.com/en/polls/DimZdfAqlWey2LpoG78pqg [admin link]  
10 votes, 8 software proposed.  
software installed: 
stacks 2.3e: http://catchenlab.life.illinois.edu/stacks/  
msprime: https://github.com/tskit-dev/msprime

**Poll #5**  
https://pollunit.com/en/polls/GxQgu_KkhIgJIXZVv-Eyxw  
https://pollunit.com/en/polls/3ry34FweQN2utcQgc1M3QQ [admin link]
12 Votes, 8 suggested software.  
software installed: 
The ORGanelle ASseMbler https://git.metabarcoding.org/org-asm/org-asm/wikis/home  
Segemehl https://www.bioinf.uni-leipzig.de/Software/segemehl  
pbzip2 installed from Fedora  
Nextflow  https://www.nextflow.io/  

**Poll #6**  
https://pollunit.com/en/polls/xadbbpjcaglhzf7mcdodnw  
https://pollunit.com/en/polls/WlR9dUQMYGYFpxt1Jz7vhw (admin)  



\
**List of software in the poll that were suggested but not selected:**  
obitools https://pythonhosted.org/OBITools/welcome.html   
CAFE [Computational Analysis of Gene Familiy Evolution] https://github.com/hahnlab/CAFE  




# User feedback.
People from the Computational Biology Group (DBC) think this is a useless
waste of time.

